﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crush : MonoBehaviour
{
    private static Color selectedColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
    private static Crush previusSelected = null;

    private SpriteRenderer spriteRenderer;
    private bool isSelected = false;
    public int id;

    private Vector2[] adjacentDirections = new Vector2[]
    {
        Vector2.up,
        Vector2.down,
        Vector3.right
    };


    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

   private void SelectCrush()
    {
        isSelected = true;
        spriteRenderer.color = selectedColor;
        previusSelected = gameObject.GetComponent<Crush>();
    }
    private void DeselectCrush()
    {
        isSelected = false;
        spriteRenderer.color = Color.white;
        previusSelected = null;
    }

    private void OnMouseDown()
    {
        if(spriteRenderer.sprite == null ||
            BoardManager.sharedInstance.isShifting)
        {
            return;
        }

        if (isSelected) //seleccion del crush
        {
            DeselectCrush(); 
        }
        else
        {
            if(previusSelected == null)
            {
                SelectCrush();
            }
            else
            {
                if (CanSwipe())
                {
                    SwapSprite(previusSelected);
                    previusSelected.FindAllMatches(); //comprueba la destruccion 
                    previusSelected.DeselectCrush();
                    FindAllMatches();

                    GUIManager.sharedInstance.MoveCounter--;

                    
                    

                }
                else
                {
                    previusSelected.DeselectCrush();
                    SelectCrush();
                }

            }
        }
    }
    public void SwapSprite(Crush newCrush)
    {
        if(spriteRenderer.sprite == newCrush.GetComponent<SpriteRenderer>().sprite)
        {
            return;
        }
        Sprite oldCrush = newCrush.spriteRenderer.sprite;
        newCrush.spriteRenderer.sprite = this.spriteRenderer.sprite;
        this.spriteRenderer.sprite = oldCrush;

        int tempId = newCrush.id;
        newCrush.id = this.id;
        this.id = tempId;


    }
    private GameObject GetNeighbor(Vector2 direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, direction);

        if (hit.collider != null)
        {
            return hit.collider.gameObject;
        }
        else
        {
            return null;
        }

    }
    private List<GameObject> GetAllNeighbors()
    {
        List<GameObject> neighbors = new List<GameObject>();

        foreach(Vector2 direction in adjacentDirections)
        {
            neighbors.Add(GetNeighbor(direction));

        }
        return neighbors;
    }
    private bool CanSwipe()
    {
        return GetAllNeighbors().Contains(previusSelected.gameObject);

    }

    private List<GameObject> FindMatch(Vector2 direction)
    {

        List<GameObject> matchingCrushes = new List<GameObject>();

        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, direction);

        while(hit.collider != null &&
            hit.collider.GetComponent<SpriteRenderer>().sprite == spriteRenderer.sprite)
        {
            matchingCrushes.Add(hit.collider.gameObject);
            hit = Physics2D.Raycast(hit.collider.transform.position, direction);
        }



        return matchingCrushes;
    }


    private bool ClearMatch(Vector2[] directions)
    {
        List<GameObject> matchingCrushes = new List<GameObject>();

        foreach(Vector2 direction in directions)
        {
            matchingCrushes.AddRange(FindMatch(direction));
        }
        if(matchingCrushes.Count >= BoardManager.MinCrushesToMatch)
        {
            foreach(GameObject crush in matchingCrushes)
            {
                crush.GetComponent<SpriteRenderer>().sprite = null;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void FindAllMatches()
    {
        if(spriteRenderer.sprite == null)
        {
            return;
        }
        bool hMatch = ClearMatch(new Vector2[2]
        {
            Vector2.left, Vector2.right
        });

        bool vMatch = ClearMatch(new Vector2[2]
        {
            Vector2.up, Vector2.down
        });

        if(hMatch || vMatch)
        {
            spriteRenderer.sprite = null;
            StopCoroutine(BoardManager.sharedInstance.FindNullCrushes());
            StartCoroutine(BoardManager.sharedInstance.FindNullCrushes());
        }
    }
    
}
